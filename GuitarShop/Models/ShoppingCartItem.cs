﻿

namespace GuitarShop.Models
{
    public class ShoppingCartItem
    {
        public int Id { get; set; }

        public int Quantity { get; set; }

        public Guitar Guitar { get; set; }
    }
}
